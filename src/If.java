
public class If {
	public static void main(String[] args) {
		int m = 95;
		if(m >= 90 )
		{
			System.out.println("sudhama got distinction");
		}
		if(m >= 75 && m < 90)
		{
			System.out.println("first class");
		}
		if(m >= 50 && m < 75)
		{
			System.out.println("second class");
		}
		if(m >= 40 && m < 50)
		{
			System.out.println("third class");
		}
	
		if(m < 40  )
		{
			System.out.println("failed");
		}
		
	}

}
