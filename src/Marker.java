package basics;

public class Marker implements Comparable{
	private double price;
	public final String brand;
	String color;
	static String category;
	
	@Override
	public int compareTo(Object temp) {
		Marker that = (Marker)temp;
		
		return this.brand.compareTo(that.brand);
	}
	
	public Marker() {
		/*price = 25.0;
		brand = "Camlin";
		color = "Black";*/
		
		this("Camlin");
	}
	
	public Marker(double price) {
		brand = "Camlin";
		color = "Black";
		this.price = price;
	}
	
	public Marker(String b) {
		this(b, "Black");
		/*price = 25.0;
		brand = b;
		color = "Black";*/
	}
	
	public Marker(String b, String c) {
		this(b, c, 25.0);
	}
	
	public Marker(String brand, String color, double price) {
		this.brand = brand;
		this.color = color;
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "Marker[" + brand + ", " + color + ", " + price + "]";
	}
	
	@Override
	public Marker clone() {
		Marker temp = new Marker(brand);
		temp.price = price;
		temp.color = color;
		return temp;
	}
	
	public void write(String s) {
		System.out.println(s);
	}
	
	public void write(int v) {
		System.out.println(v);
	}
	
	public void write(double d) {
		System.out.println(d);
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) throws MarkerException{
		if(price > 0) {
			this.price = price;
		}else {
			throw new MarkerException("For price: " + price);
		}
	}
	
	/*public void setPrice(double price) throws IllegalArgumentException{
		if(price > 0) {
			this.price = price;
		}else {
			//System.out.println("Invalid price for Marker");
			
			IllegalArgumentException iae = new IllegalArgumentException();
			throw iae;
		}
	}*/
}
