package basics;
import java.lang.*;
import java.io.*;
public class Reverse 
{
 
public int reverse(int num)
{
int rev = 0;
while(num != 0)
{
rev = (rev*10)+(num%10);
num = num/10;
} 
 return rev;
}
public static void main(String a[])
{
Reverse r = new Reverse();
System.out.println("Reversed Number : " + r.reverse(15678));
}
}