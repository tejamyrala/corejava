package Com.guruofjava.sudaksha.j45.collection;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ListDemo {
	public static void main(String[] args) {
		List<String> t1 = new ArrayList(Arrays.asList("James", "Gosling", "Created", "Java", "Language"));
		System.out.println(t1);
		String str = " ";
		String[] values = t1.toArray(new String[0]);

		for (int i = 0; i < values.length; i++) {
			str = str + " " + values[i];
			
		}
		str = str.trim();
		System.out.println(str);
	}
}
