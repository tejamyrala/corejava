package Com.guruofjava.sudaksha.j45.exception;

public class ExceptionsDemo {
	public static void main(String[] args) {
		System.out.println("start of main");
		m1();
		System.out.println("end of main");
	}
	public static void m1() {
		System.out.println("start of m1");
		
		m2();
		
		System.out.println("end of m1");
		
	}
		
	public static void m2( ) {
		System.out.println("start of m2");
		try {
		int i = Integer.parseInt("u6");
		System.out.println(Math.sqrt(i));
		}
		catch(NumberFormatException nfe) {
			System.out.println("Solved NumberFormatException");
		}
		
		
		System.out.println("end of m2");
		
		
}
}

