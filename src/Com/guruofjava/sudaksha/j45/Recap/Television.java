package Com.guruofjava.sudaksha.j45.Recap;

public class Television {

	private final byte MAX_VOLUME = 100;
	private final short MAX_CHANNEL = 2000;
	public final String BRAND_NAME;
	public final double SIZE;
	private double price;
	private byte volume;
	public short channel;

	public void setChannel(short channel) {
		if (channel > 0 || channel < MAX_CHANNEL) {
			this.channel = channel;
		}
	}

	public void nextchannel() {
		if (channel < MAX_CHANNEL) {
			channel++;

		}
	}

	public byte getVolume() {
		return volume;
	}

	public void decreaseVolume() {
		if (volume > 0)
			volume--;
	}

	public void increaseVolume() {
		if (volume > MAX_VOLUME)
			volume++;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if (price >= 4000) {
			this.price = price;
		} else {
			System.out.println("INVALID PRICE");
		}
		this.price = price;
	}

	/*public Television() {
		BRAND_NAME = "Sony";
		SIZE = 20;
		price = 15000;
	}*/

	public Television(String b) {

		BRAND_NAME = b;
		SIZE = 20;
		price = 15000;
	}

	public Television(String b, double s) {

		BRAND_NAME = b;
		SIZE = s;
		price = 25000;
	}

	public Television(String b, double s, double p) {

		BRAND_NAME = b;
		SIZE = s;
		price = p;

	}
}
