package Com.guruofjava.sudaksha.j45.Recap;

public class SmartTv extends Television{
	public final String SOFTWARE_NAME;
	
	public SmartTv() {
		super("samsung");
		SOFTWARE_NAME = "Default";
	}
	public SmartTv(String software) {
		super("samsung");
		SOFTWARE_NAME = software;
	}
}
