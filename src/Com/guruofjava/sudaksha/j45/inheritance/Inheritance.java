package Com.guruofjava.sudaksha.j45.inheritance;

public class Inheritance {
	public static void main(String[] args) {
		Animal a1 = new Animal();
		Animal a2 = new Cat();

		a1.move(7);

		Cat c1 = new Cat();
		c1.move(20);

		Bird b1 = new Bird();
		b1.move(66);
		
		
		Fish f1 = new Fish();
		f1.move(55);
		
		
		a1.WhoAmI();
		a2.WhoAmI();
		c1.WhoAmI();
		a1.move(500);
//		a1.hunt();
	
		
	}	




}
