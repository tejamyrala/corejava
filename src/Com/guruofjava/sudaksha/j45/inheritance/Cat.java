package Com.guruofjava.sudaksha.j45.inheritance;

public class Cat extends Animal implements Hunter  {
	
	public void move(int distance){
		System.out.println("Cat is moving "+ distance + " distance");
	}
	
	public static void whoAmI(){
		System.out.println("Cat");
	}
	
	public void hunt(){
		System.out.println("Cat is hunting");
	}
	
	
	public void run(){
		System.out.println("Cat can run...");
	}
	
	
	public void jump(){
		System.out.println("Cat can jump...");
	}

}

	
	
