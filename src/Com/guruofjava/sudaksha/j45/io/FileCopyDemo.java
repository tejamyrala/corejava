package Com.guruofjava.sudaksha.j45.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

public class FileCopyDemo {
	public static void main(String[] args) {
		FileInputStream in = null;
		FileOutputStream out = null;

		try {

			in = new FileInputStream( "J:\\Friend\\friend.txt" );
			out = new FileOutputStream("J:\\Friend\\friend.txt", true);

			byte[] data = new byte[50];
			int i;

			while( (i = in.read(data)) > 0 ) {
				System.out.print(new String(data, 0, i));
				
				out.write(data, 0, i);
			}

		} catch (FileNotFoundException fnfe) {

		} catch (IOException ioe) {

		} finally {
			// if (in != null) {
			if (Objects.nonNull(in)) {
				try {
					in.close();
				} catch (IOException ioe) {

				}
			}

			if (Objects.nonNull(out)) {
				try {
					out.close();
				} catch (IOException ioe) {

				}
			}
		}
	}

	public static void main1(String[] args) {

		FileInputStream in = null;
		FileOutputStream out = null;

		try {

			in = new FileInputStream( "J:\\Friend\\friend.txt" );
			out = new FileOutputStream( "J:\\Friend\\friend.txt" );

			int i;

			while ((i = in.read()) != -1) {
				System.out.print((char) i);

				out.write(i);
			}

		} catch (FileNotFoundException fnfe) {

		} catch (IOException ioe) {

		} finally {
			// if (in != null) {
			if (Objects.nonNull(in)) {
				try {
					in.close();
				} catch (IOException ioe) {

				}
			}

			if (Objects.nonNull(out)) {
				try {
					out.close();
				} catch (IOException ioe) {

				}
			}
		}
	}
}

	


