
public class IfElse {
	public static void main(String[] args) {
		int m = 84;
		if(m >= 75) {
			System.out.println("distinction");
		}else {
			if(m >= 60) {
				System.out.println("first");
			}else {
				if(m >= 50) {
					System.out.println("second");
				}else {
					if(m >= 40) {
						System.out.println("third");
					}else {
						if(m < 40) {
							System.out.println("failed");
						}
					}
				}
			}
		}
	}

}
